# Створіть клас, який описує книгу. Він повинен містити інформацію про автора, назву, ріквидання та жанрі. Створіть
# кілька книжок. Визначте для нього операції перевірки нарівність та нерівність, методи __repr__ та __str__.

class Book:
    def __init__(
            self,
            year: int,
            name: str,
            author: str,
            genre: str
    ):
        self.year = year
        self.name = name
        self.author = author
        self.genre = genre
    def __eq__(self, other):
        if not isinstance(other, Book):
            raise TypeError(f"type of {other} is {type(other)}, needs type Book")
        return self.name == other.name and self.author == other.author
    def __ne__(self, other):
        if not isinstance(other, Book):
            raise TypeError(f"type of {other} is {type(other)}, needs type Book")
        return self.name != other.name or self.author != other.author
    def __str__(self):
        return f"book`s name: {self.name}\nauthor: {self.author}\nyear: {self.year}"
    def __repr__(self):
        return f"book`s name: {self.name}\nauthor: {self.author}\nyear: {self.year}"

